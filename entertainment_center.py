import media
import fresh_tomatoes

toy_story = media.Movie("Toy Story",
                        "A story of ...",
                        "https://upload.wikimedia.org/wikipedia/en/1/13/Toy_Story.jpg",
                        "https://www.youtube.com/watch?v=KYz2wyBy3kc")


#print(toy_story.storyline)

avator = media.Movie("Avatar",
                     "A marine on an alien planet",
                     "https://upload.wikimedia.org/wikipedia/en/b/b0/Avatar-Teaser-Poster.jpg",
                     "https://www.youtube.com/watch?v=5PSNL1qE6VY")

#print(avator.storyline)

#avator.show_trailer()
print(media.Movie.VALID_RATINGS)
print(media.Movie.__doc__)
print(media.Movie.__name__)
print(media.Movie.__module__)

#movies = [toy_story, avator]
#fresh_tomatoes.open_movies_page(movies)


